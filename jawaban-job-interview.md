# No 1
![](https://gitlab.com/azharhaikal38/praktikum-sistem-multimedia/-/raw/main/RekamWajah.gif)
##### Link Youtube : https://youtu.be/SZpWmB3peZI

# No 2
##### Aplikasi ini berbasis CLI hanya dapat digunakan apabila melakukan perintah input output dari keyboard

```mermaid
flowchart TD
    A[start] --> B(openCamera)
    B --> C{while}
    C --> D[Break]
    C --> E[cam.read]
    C --> F[eye detector]
    E --> G[cvt color] 
    F --> H[in eye x,y,w,h]
    G -->F 
    F -->D
    H -->I[rectangle frame]
    I -->J[imwrite]
    J -->K{fetch data}
    K -->L[Break]
    K -->M[Face id input]
    M -->N[Print pengambilan data selesai]
```
# No 3

##### Aspek kecerdasan buatan dalam aplikasi ini adalah menggunakan algoritma haar cascade clasifier, yang digunakan untuk mendeteksi wajah. Algoritma ini mampu mendeteksi dengan cepat dan realtime sebuah benda termasuk wajah.

# No 4
![](https://gitlab.com/azharhaikal38/praktikum-sistem-multimedia/-/raw/main/efekgitar.gif)
##### Link Youtube : https://youtu.be/NRPKtSDOsfk

# No 5 
##### Aplikasi ini Berbasis GUI yang dapat memungkinkan pengguna berinteraksi dengan perangkat keras serta memudahkan pengguna dalam mengoperasikan sebuah sistem

```mermaid
flowchart TD
    A[start] --> B(Load Gitar.wav)
    B --> C[distortion y]
    C --> D[N = len x]
    D --> E{for i in range}
    E --> F[y.append 2*x i]
    E --> H[y.append 3-2 -x i *3**2*3]
    E --> I[y.append 1]
    H --> J[return y]
    F --> J
    I --> J
    J --> K[sf.write]
    K --> L[def.plot]
    K --> M(def.plot ori)
    K --> N(def_play ori)
    K --> O(def.stop)
    L --> P(button play)
    M --> P 
    N --> P 
    O --> P
```

# No 6 
##### Aspek kecerdasan buatan pada aplikasi ini adalah dengan menggunakan metode signal  processor dimana DSP ini bekerja mengubah sinal analog menjadi sinyal diskrit oleh sebuah komponen yang bernama ADC atau Analog to Digital Converter sebelum nantinya akan diolah oleh Digital Signal Processor itu sendiri. 

# No 7
![](https://gitlab.com/azharhaikal38/praktikum-sistem-multimedia/-/raw/main/virtualkybrd.gif)
###### Link Youtube : https://youtu.be/xbOoPZlIhyo

# No 8
###### Aplikasi Virtual keyboard bertujuan untuk memudahkan user jika terjadi kerusakan keyboard yang dimiliki oleh user, sehingga dapat menggunakan aplikasi ini untuk sementara waktu.

```mermaid
flowchart TD
    A[Start] --> B[open cam]
    B ---> open_virtual_keyboard ---> C
    B --> C{hand detection }
    C -->|One| D[hand land marker]
    C -->|Two| E[type click 12. midle_finger_tip 8.index_finger_tip ]
    D --> F[clicking on a keyboard button]
    E --> F
    F --> G[output type keyboard in screen]
```
# No 9 
###### Aspek kecerdasan buatan pada aplikasi ini menyediakan mekanisme input alternatif untuk pengguna penyandang disabilitas yang tidak dapat menggunakan keyboard konvensional, atau untuk pengguna dwi-atau multi-bahasa yang sering beralih di antara set karakter atau abjad yang berbeda, dan mempermudah user yang sedang dalam masalah pada komponen keyboard konvensionalnya sehingga dapat menggunakan aplikasi ini untuk sementara waktu.Meskipun keyboard perangkat keras tersedia dengan tata letak keyboard ganda (misalnya huruf Cyrillic / Latin di berbagai tata letak), keyboard di layar menyediakan pengganti yang praktis .
  


# No 10
![](https://gitlab.com/azharhaikal38/praktikum-sistem-multimedia/-/blob/main/Untitled_Diagram-Page-3.jpg)
###### Pada rancangan produk digital berbasis pemrosesan video yang didalamnya mengandung unsur Video AI, yang bertujuan membuat sebuah produk Virtual keyboard. di dalam pemrosesan produk tersebut terjadi interaksi antara user dan virtual keyboard.


   



  

